<?php

    // -- Class Name : OpenWeather
    // -- Purpose : Display weather conditions per countries capitals with 12h period
    // -- Created On : 05.2016, 
    class OpenWeather {
        public static $capital = array(
        'PL' => '6695624', 
        'RU' => '524901', 
        'CZ' => '3067695', 
        'FR' => '6618626',
        'DE' => '2950159',  
        'IT' => '3169070',
        'ES' => '6359304',
        'PT' => '6458923',
        'RO' => '683508',
        'FI' => '658226',
        'NL' => '2759794',
        'BG' => '727011',
        'SE' => '2673722',
        'TR' => '323786',
        'HR' => '3186886',
        'GB' => '2643743',
        'AE' => '8056564',
        'AR' => '6559994',
        'AT' => '2761367',
        'BE' => '3337389',
        'BR' => '3410315',
        'CA' => '4276816',
        'CH' => '7285212',
        'CL' => '3871336',
        'DK' => '2618425',
        'EE' => '588409',
        'GR' => '264371',
        'HU' => '7284844',
        'IE' => '7778677',
        'IL' => '293397',
        'IN' => '1261481',
        'LT' => '593116',
        'LU' => '2960316',
        'LV' => '456173',
        'MX' => '3996063',
        'MY' => '1733046',
        'NO' => '6453366',
        'PA' => '3703443',
        'PE' => '3936456',
        'SE' => '2673730',
        'SG' => '1445156',
        'SI' => '3196359',
        'SK' => '3060972',
        'TH' => '1609350',
        'VE' => '3646738',
        'ZA' => '964137'
          );
        // timezone
         public static $timezone = array(
        'PL' => 'Europe/Warsaw', 
        'RU' => 'Europe/Moscow', 
        'CZ' => 'Europe/Prague', 
        'FR' => 'Europe/Paris',
        'DE' => 'Europe/Paris',  
        'IT' => 'Europe/Rome',
        'ES' => 'Europe/Madrid',
        'PT' => 'Europe/Lisbon',
        'RO' => 'Europe/Bucharest',
        'FI' => 'Europe/Helsinki',
        'NL' => 'Europe/Amsterdam',
        'BG' => 'Europe/Sofia',
        'SE' => 'Europe/Stockholm',
        'TR' => 'Europe/Istanbul',
        'HR' => 'Europe/Zagreb',
        'GB' => 'Europe/London',
        'AE' => 'Africa/Algiers',
        'AR' => 'America/Argentina/Buenos_Aires',
        'AT' => 'Europe/Vienna',
        'BE' => 'Europe/Brussels',
        'BR' => 'America/Sao_Paulo',
        'CA' => 'America/Toronto',
        'CH' => 'Europe/Zurich',
        'CL' => 'America/Santiago',
        'DK' => 'Europe/Copenhagen',
        'EE' => 'Europe/Tallinn',
        'GR' => 'Europe/Athens',
        'HU' => 'Europe/Budapest',
        'IE' => 'Europe/Dublin',
        'IL' => 'Asia/Jerusalem',
        'IN' => 'Asia/Kolkata',
        'LT' => 'Europe/Vilnius',
        'LU' => 'Europe/Luxembourg',
        'LV' => 'Europe/Riga',
        'MX' => 'America/Mexico_City',        
        'NO' => 'Europe/Oslo',
        'PA' => 'America/Asuncion',
        'PE' => 'America/Lima',        
        'SG' => 'Asia/Singapore',
        'SI' => 'Europe/Ljubljana',
        'SK' => 'Europe/Bratislava',       
        'VE' => 'America/Caracas',
        
          );
        public $user = 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx';   // Open Weather API user id         
        public $path = 'stored/';
        public $imgUrl = 'img/';
        public $data = array();
        


        function __construct($ctry){
            $loc = array_key_exists($ctry,self::$capital) ? self::$capital[$ctry] : self::$capital['GB'];            
            $timezone = array_key_exists($ctry,self::$timezone) ? self::$timezone[$ctry] : self::$timezone['GB'];
            date_default_timezone_set($timezone);
            
            $this->getData($loc,$ctry,3); //get weather for next 3 days           
            
        }

        

// -- Method Name : getData
// -- Params : $loc - capital code,$ctry,$day,$int - 3h period (8*3h = 24h) 
// -- Purpose : get & store weather data from http://openweathermap.org/api
        function getData($loc,$ctry,$days) {
            $path = $this->path;
            $lang = ($ctry == 'PL') ? '&lang=pl' : '';
            $file = $ctry.".txt";
            $p = $path.$file; 
            $now =  time();
            $localtime = getdate($now);  
            $user = $this->user;            
            $imgUrl = $this->imgUrl;
            $url = "http://api.openweathermap.org/data/2.5/forecast/city?id=".$loc.$lang."&units=metric&APPID=".$user."&format=json";
                      
            
            if (file_exists($p)){
              
                if (($now - filemtime($p)) > 43200) {  // czas Ĺźycia danych (domyślnie 12h)
                        
                        if(!$getw = @file_get_contents($url)) { // czy poprawnie zaciąga
                          echo 'Load failed';
                          $getw = file_get_contents($path.$file);
                        }                
                        file_put_contents($p,$getw);
                        
                } else { // nie jest starszy niż 12h
                       $getw = file_get_contents($p); echo '<br> pobiera lokalnie ';
                        
                      }               
            } else  { // nie ma w ogóle
                
                $getw = file_get_contents($url); echo '<br> pobiera z urla ';
                file_put_contents($p,$getw);
            }

            $weather = json_decode($getw,true);
            $int = 0;
            for ($day=0;$day<$days;$day++) { //deklaruje dane pogodowe: ikona,temp,opis,daty,miasto
            
              $this->data[$day]['icon'] = $imgUrl.$weather['list'][$int]['weather'][0]['icon'].".png";
              $this->data[$day]['temp'] = $this->Celsius($weather['list'][$int]['main']['temp']);
              $this->data[$day]['desc'] = $weather['list'][$int]['weather'][0]['description'];
              $dateUnix = $weather['list'][$int]['dt'];
              $d = getdate($dateUnix);
              $this->data[$day]['wday'] = $d['weekday'];
              $this->data[$day]['date'] = $localtime['month'].' '.sprintf("%02d", $localtime['mday']).' '.sprintf("%02d", $localtime['hours']).':'.sprintf("%02d", $localtime['minutes']);
              $this->data['city'] = $weather['city']['name'];
              
              $int += 8;
            }
        }    

       

// -- Method Name : Celsius
// -- Params : $val
// -- Purpose : format to Celsius grads
     public function Celsius($val) {
            return round($val)."&#176;";
        }

    }
    
   